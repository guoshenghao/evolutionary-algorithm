# 进化计算与交互式进化计算设计书
SimpleGA分支

![SimpleGA](branch.png)

记录人：郝国生

代码开始时间：2005年8月
设计书开始时间：2020年6月14日
## 序言
### SimpleGA分支存在的理由
- 从简单处找到bug
- 从简单处理解
- 从简单处扩展
 
## 1 引言
### 1.1 已有平台
已经有的进化计算平台，包括jMetal等，但发现改造jMetal比较困难，就自己动手搭一个。
### 1.2 系统结构
参考了jMetal，系统主要包括两个主体：算法Algorithm、问题Problems。
###  关于通信
需要的参数或资料等一般可以在相应的功能包中找到，当多个包共同使用时，则可以从ControlParameters中获得，这里提供了一个类似于黑板的机制。
###  类簇及关系
Individual类与问题挂钩，不同的问题具有不同的Individual。在实际运行时，给出Individual的类型。
### 1.3 本系统的优点
**开源**：https://gitee.com/guoshenghao/Interactive-and-Evolutionary-Algorithms/tree/simpleGA/
Robustness：每k代就有一个备份，因为一些复杂的高维问题耗时。正因为有了备份功能，才在可能断电、死机、outof memory的时候，有保存的结果，可以直接在原来的基础上恢复现场继续运行。利用了数据库，数据库能高效保存现场数据，并方便恢复数据。
**Easy use**：易用性体现在导出数据直接在Excel中；体现在种群规模参数可以一次设置多个；而不用每次设置一个；实验完成后，再重新设置下一个；在配置较低的机器上也可以运行。
**高效**：不允许在同一种群中有重复的个体出现。不允许被访问过的个体再被访问机制也加入了进来。
可以指定算法运行次数，然后记录运行过程中最优个体变化，以及运行时间等。
实验结果可以选择保存到Excel文件中，也可以保存到数据库中。
### 1.4 缺点
函数式编程的缺点。虽然优点比较明显，即不对传入的对象做修改，但是缺点也明显，那就是，如果返回的结果是一个对象的话，那么每次调用函数都会新增对象，因此，当迭代次数很多时，就会增加内存，容易产生out of memory的错误。
正是考虑到上述缺点，本系统的选择、变异等操作不采用函数式编程，即直接对传入的对象做改变。而对于交叉操作，则由于parent是随机选择的，则采用函数式编程，而不对原parent进行改变。
### 1.5 一些约定
- 在构造函数中完成初始化工作。
- 求最大值方法。本系统采用求取最大值的方法。因此，如果要利用本系统求取最小值时，需要转换换为求最大值的问题


 
## 2 算法
### 2.1 EA的核心问题
核心问题是解的生成问题。
生成第T代解的依据是{x,f(x),(t=1,2,…,T-1)}这一带标注的数据集。
### 流程
算法主要的流程包括：初始化、evaluate、排序、求最优解、选择、交叉、变异、用最优解替换一个随机解、再evaluate，接排序等直到达到算法结束条件。
### 交叉算子

### 选择算子
	选择算子没有基于排序。

### 变异算子
	根据规定的范围，变异会自动判断解的合法性。交叉算子不用判断其合法性，因为交叉算子不会越界。
	[徐莉]()提出实数变异不应该只加一个(0,1)之间的随机数，还应考虑减去一个随机数。据此，改变了DoubleDecisionVariable中的mutate方法
### 种群
#### 种群初始化
种群初始化方法一般有两种情形{杨启文}：a)随机初始化；b)均匀初始化。随机初始化方法简单，但会导致候选解在解空间的有些区域分布“密集”，有些区域分布“稀疏”。一旦最优解区域无法得到有效采样，则会在一定程度上延长最优解的发现时间。
均匀初始化方法有三种：正交实验法{蔡自兴, 2010 #3564}、随机均匀设计法{陈明华, 2010 #3565}和佳点集法{龙文, 2012 #3566}。这三种方法都能对解空间进行均匀采样，但正交实验法在高维空间内需要较多的采样点{张铃, 2001 #3568}，因此，正交实验法更适合应用于低维问题的优化方案中。随机均匀设计法与佳点集法的效果相差不大。故本文采用佳点集法进行种群初始化。
下图是随机初始化方法和佳点集初始化方法分别在边长为1的二维正方形区域内100个采样点的分布情况。对比可以看出，佳点集产生的点在同样的正方形区域内分布更加均匀，这对于增加候选解的多样性、防止“早熟收敛”有着重要的意义。
![](figures/randInit.png) 
(a)随机初始化
![](figures/goodpoint.png) 
（b）佳点集初始化

	
### 全局唯一性对象
在本系统中，如果某对象是全局唯一性的，则不需要作为参数传递，可以直接作为静态对象获取。
满足这些条件的对象包括：
	具体的算法。在实际运行过程中，不会让多个算法同时执行。如遗传算法与粒子群算法不会同时作为两个算法执行，即只会有一个算法在一个时刻执行，因此这是全局唯一性对象。
	具体的问题。在实际运行过程中，不会让算法去同时优化多个问题，只会针对一个问题进行优化，如果涉及到多个问题，则分别独立优化，而不会出现在同一个JVM中。因此，这也是全局唯一性对象。
	与算法伴随的具体策略。
因此，当其他对象或方法需要具体的算法或具体的问题时，则可以直接从静态方法中获取，而不必传递参数。
 在使用这种对象时，需要注意的是：在每次重新开始实验后，要对其成员进行初始化。如果不初始化，则上一次实验的内容会影响本次实验。
###  Exploration和Exploitation
###  种群按适应值排序
~~Java提供了TreeSet，作为一种排序的数据结构，因此，Population类可以直接使用TreeSet来维护Individual。
TreeSet有两个重要的特点：一个是Tree，另一个是Set。Tree是一个有序树，如果是二叉树，则左子树上的所有结点的值都比右子树上所有结点的值小。Set是集合，其中的个体无重复！！这意味着，Population中自动成为无重复的个体。自动实现了NoReappearOntheSamePopulation的功能。当然这个set借助Tree的功能，成为有序的了。
借助TreeSet上面的特点，Population可以避免实现Sort方法，可以避免实现checkReappearance。而历史个体也可以借助其适应值的Compare方法，实现有序无重复存储。
但是需要注意的是：在实现compareTo时，如果只用适应值进行对比，那么对于非单调的优化问题，很多解会被认为相等；为了避免这种情况，在compareTo方法中加上决策变量的对比；即决策变量与目标变量都相等，才算相等。
TreeSet默认是升序，有pollFirst方法可以被调用把最差个体移除；有poolLast方法把最优个体移除。
但是，选择算子后得到的交叉与变异候选池则需要有重复的，因此，在选择算子中，还需要用List。 ~~
###  可以被调节的参数
###  关于三角选择算子
删除了cos和ctan选择算子，因为sin和tan具备了相应功能了。 
###  关于交叉点个数
一点交叉和多点交叉，都是生成新个体的方法。虽然多点交叉似乎由于交叉点个数增多，会增加解的多样性。但搜索空间中，多样性的度量中所依赖的解的不同性的概念，并不依赖于有多么不同，即只要两个解编码不同，就认为不同，因此在计算多样性时贡献相同。另外，在搜索空间中，解的分布性也并不会因为交叉点个数的增加而增加。所以结论是：多点交叉与一点交叉没有本质的区别，因此，删除了多点交叉。
###  最优个体保留
在Populaion中有属性bestIndividual和worstIndividual，用来保存当前种群的最优和最差个体，在HistoryIndividuals中有bestSofar和worstSofar用来保存进化到当前的最优和最差个体。

~~关于保留最优个体的方法，如果采用替换第0个个体的办法，那么有如下一个缺点：由于第0个个体是进化操作后生成的，当采用历史个体不再重复出现的策略时，它已经进入了历史个体，因此它不会被评价，也不会再次出现。如果它是比当前最优解还好的解，那么算法的性能因此会有所损失。
针对这个缺点，我们采用的方法包括两步骤：
步骤1：在变异时少生成一个个体；
步骤2：最优保留时，把最优的个体插入到链表前面。
	最优个体保留到新的一代的目的是在选择算子中使用，所以在选择前执行，至于从外观指标上来说，则是用于监控算法用的。
在AbstractAlgorithm中，表面看起来，会使种群规模增1，从而增加了较差个体的生存压力。但是，由于population采用的数据结构是TreeSet<>，所以当加入的最优个体已经存在于population中的时候，种群规模并不会增1。因为是在Set中不允许出现重复个体。 
public void reserveBestInd(Population population) {
        population.getIndividuals().add(HistoryIndividuals.getInstance().getHistory4Parent().last().getIndividual().clone());
    }~~

###  最优个体保留的方法
最优个体保留有两个地方有：第一个地方是选择算子执行前，第二个地方是进化操作完成后。第二个地方的目的是为了输出，记录每代的最好结果。
###  最差个体集合
采用hashSet数据结构。不限制容量。
###  最优个体与最差个体求取时间
由于算法执行不需要对种群排序，所以，在每次求取适应值时，就更新最优个体与最差个体，而不是调用sort方法。 
###  算法迭代
controllers.Connection4SetAndRunning类中的方法beginEvolving中while循环中的控制变量是由3个条件决定的：
1）population.getBestIndividual().compareTo(stopFitnessIndividual) >= 0 
2）generationNum * algorithm.getPopulationSize() >= ControlParameters.stopEvaluationNumber
3）genrationNum - fitnessSteps.get(fitnessSteps.size() - 1) > 500 * population.getIndividuals().size()
###  个体编码
####  编码涉及的类层次
本项目中individual的编码有多种格式，如二进制，浮点型等。对应的population也有这些类型。再往上，对应的search space也有多种类型。
一致性问题：individual初始化时采用二进制，而population却使用浮点类型；同样，对于search space也会存在这种情况，怎么办？如何避免这种不一致性？
考虑到individual是population的元素，而后者又是search space的子类，参考社会工作中，不能越级办事的规律，如果想得到individual，则请求population帮忙，而population是归search space负责管理的，则需要请求search space帮忙；而search space是与problem挂钩或属于后者的，因此，应该请后者帮忙。这样一个problem面对algorithm时，是一个统一的整体。
或者说是一个唯一的”前台”，而不是一个”入口”。即可以在前台这儿等待后边给办理，而不是进入里边去办理（NTU的人事部门办理的方法）。这也是本项目只能看到两个主要的包的原因，这两上包是problem和algorithm。
二进制编码的另一个优点是不用考虑越界的问题，无论采用什么样的交叉或变异算子。
###  知识利用
选择利用了种群内个体间的比较知识；
交叉受生物有性繁殖启发，利用了两个体携带的基因知识；
变异受生物中变异启发，利用了单个体携带的基因知识；
分布估计算法（EDA）利用了种群内所有等位基因的统计比较知识；

 
##  3 Problem
Problem簇类与Individual簇类的联系只有一个方面：目标空间的个数（单目标优化或多目标优化），但不能限定Individual的类型，包括决策变量数组的类型（浮点数或字符串）。
### 编码
每个problem可以有多种编码格式，如二进制是一种万能的编码格式，即可以用来处理实数，也可以用来处理字符串。
但是为每个problem设置多种编码格式，会因此要设置多种种群初始化方法，多种搜索空间检索化方法，这些设置的复杂性和易错性，可能会抵消掉采用多种编码格式带来的炫技好处（除了妶技，好像没有太多的其他好处）编码方法之间有区别，例如二进制编码，对其高位和低位进行变异时，其exploeration和exploitation效果不同；而实数编码的变异则需要另外考虑。
为此，为每个问题只设置一种编码格式，从而在界面上，用户不必选择编码格式，而是在定义问题时（在Controlparameter类中），直接指定编码格式。
#### 关于实数编码与二进制编码的变异
变异操作在这两种编码上带来的效果差异很大。
在实数编码上，一般可以在各维上增加或减少一个随机数rand.nextDouble()，实现变异。
在二进制编码上，一般通过0和1的反转，实现变异。
表面上看起来，都实现了变异，但是对于二进制来说，这种变异中既包括exploitation,也包括exploration。因为在高位变异，对应的实数变异前后差距会很大，所以是exploration；相反，在低位变异，对应的实数变异前后差距不会太大，所以是exploitation。然而，对于实数编码来说，前述变异中，则只包括了exploitation，而没有包括exploration。因此，编码的选择，要根据具体情况进行选择，另外，也要在算法中做相应的处理，例如对exploration和exploitation的调节与控制等。
#### 竟争交叉与变异
为了实现灵活的exploration与exploitation的动态调整，采用crossover与mutation竞争执行的方法。这样，mutation的概率与crossover的概率不必设置。
要求种群规模大于2；否则竞争时，交叉个体数目会太少。
###  测试函数
#### 来源
在下面的链接中提供了许多benchmark，可以借鉴。
http://benchmarkfcns.xyz/fcns
https://www.cs.unm.edu/~neal.holts/dga/benchmarkFunction/
#### 系统已经包含的函数
[具体内容见](files/function.docx)
#### 决策变量与个体
以前只有一个类Individual，这个类中包括的属性有决策变量和适应值。决策变量的类型有Double，String，BinaryCode等。因此，对应的Problems系列类也有对应的这些类型，另外，对应的Population也有对应的这些类型。
由于TreeSet的引入，其Set特质之一是元素不重复，而这种不重复实现所依赖的方法是equal方法。Indivdual的equal需要判断两个对象的适应值是否相同，但是适应值相同，不一定是同一个个体，因为他们的决策变量可能不同。而如果在equal中加入决策变量因素，则又无法直接利用适应值，因此，决定把决策变量独立出来，新建一个包decisonVariable，在其中加入了接口，抽象类及相关类型，这些类型包括：Double，String，BinaryCode。这样使得Individual类不必再判断决策变量的类型。
在此基础上，进一步处理Population，原来该包中也包括Double，String，BinaryCode等多种类型，现在发现他们不同之处仅在于对Individual的初始化方法，而初始化方法可以委托给决策变量，因此，Population也不必包含这些类型了。 
实现上述目标的过程中，使用了泛型方法，这其中有一个问题是如何根据模型类型实例化一个对象。

 
## 4 实验与运行
### 实验运行配置文件
运行时配置文件位于resources目录下，以Controlparameters开头，后接数字，该数字即是对应的测试函数编号。该文件以conf作为扩展名的文本文件。文件中包括相关配置说明。
运行时的参数配置依赖于该文件。如遗传算法中的交叉点个数与变异点个数，依赖于该文件中的dimension，分别等于该值减1和0。
参数配置中需要注意的是：种群规模要大于2，因为在选择算子的类SelectionCommonFunction中规定：
proUpLowNo[proUpLowNo.length - 1] = proUpLowNo[proUpLowNo.length - 2] + (proUpLowNo[proUpLowNo.length - 2] - proUpLowNo[proUpLowNo.length - 3]) / 10;
其中数组proUpLowNo用于保存轮盘赌概率区间，种群规模为2，则下标最大值为3。上面这句话中用到了下标3，如果种群规模小于2，那么会出现求下标为负的数组索引。
### 实验数据
####  借助Excel
### 多次写入
在写入实验数据到Excel文件中时，为什么反复调用写入文件，而不是一次性地写入？这是因为实验过程中不断产生数据，而这些数据需要及时地写到文件中，如果一次性写入会在内存中保存大量的数据，从而容易引起heap space out of memory的问题。
### 数据内容
实验数据中第1行至第6行的含义在D2AverageFormular2Cell中有一些描述，可以查看
第1行是运行时间，单位ms
第2行是阶跃情况，目前还没使用这个指标
第3行禁忌域的大小，我们现在没有使用这个指标
第4行是最优解所对应的X值
第5行是最优适应值maxf(X)
第6行是否hit最优解
第7行是找到最优解时或进化结束时的进化代数
### 一些约定
不同的种群规模|P|用不同的sheet，每个sheet的名称即为种群规模的大小的数字。
每个sheet中的前n-1列代表实验运行过程中最优解的值，最后一列，即第n列，是平均值。
如果算法在达到最大评价次数前就找到了最优解，那么每一列的最后会用空缺，但是Excel会自动跳过他们，不影响求平均值。
每个sheet中的记录最优适应值变化的行数的最多行数等于指定的评价次数E除以种群规模|P|。
第列的最后两行分别记录了这次实验运行的时间以及f(x)阶跃频率。
### 借助数据库
用POI把相关实验数据写到Excel文件中，经常出现java.lang.OutOfMemoryError: Java heap space的问题。采用数据库应该会避免出现这个错误。
数据库：Derby，版本10.13.1.1以上
运行模式：采用Client模式
通过ij的连接方法：connect 'jdbc:derby://localhost:1527/experimentalData;user=myeas;password=myeas';
#### 表结构
experiment
实验	create table experiment(id int not null GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) primary key, algorithm varchar(20), algorithmsParameters varchar(1000), algorithmStrategy varchar(1000), problemInfo varchar(200));	实验配置及环境变量等
experimentResults
实验结果	create table experimentResults(id int not null GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) primary key, belong2 int references experiment(id), bestFitness varchar(1000), bestSolution varchar(2000), timeSecondCost int, evaluationnumber int);	记录最优适应值，最优解，实验花费时间，实验结束时的评价次数
experimentProcess
实验过程	create table experimentProcess(id int not null GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) primary key, belong2 int references experimentResults(id), iterationNumber int, variablewithFitness LONG VARCHAR, bestfitness varchar(1000));
variablewithFitness字段中varaible与对应的fitness在一起，两者之间的分割符为”#”，种群内个体之间的分割符为“$”，个体的变量各维内部分割以“,”，适应值内部分割也是以“,”	记录实验过程，主要包括这一种群的适应值与变量

表关系如下图：
![](figures/dbExperiment.jpg)
#### 实验数据处理
处理的简捷性表达
#### 问题描述
下面的这些种群规模是在函数f23上的GAE2占优CGA的统计：2,3,4,6,7,9,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,190,200,210,230,250,260,270,280,290,300,320,330,340,350。
没人愿意看到这么密集的数据，如果能换成下面的情况，是不是就好多了。如果手工处理，那么会有32*3=96个数据集需要处理，每个3分钟，再加上可能出错，纠错2分种，那么，就需要96*5=480分钟，即8个小时。能不能编个小程序实现一下。
[2,4], [6,7], {9}, [10,170], [190,210], {230}, [250,300]
#### 代码实现
注意到结果中有两种情况，一种是整数区间，即中括号形式，一种是集合，即大括号形式。也就是说，如果连续的话，那么就是中括号，如果不连续，就用集合。 因此，先定义一个类，包容这两种形式：

```class IntegerIntervalOrSet {
private boolean interval = false;
//如果是区间，则为true；否则为false
    private List<Integer> list = new LinkedList<>();
    public IntegerIntervalOrSet() {
        interval = false;
        list = new LinkedList<>();
}
//Getor略
//Setor 略
}
```
针对区间或集合，定义它们的字符串格式
```private String outputList(IntegerIntervalOrSet list) {
        StringBuilder sb = new StringBuilder();
        if (list.isInterval()) {//是区间，只输出第1个和最后一个元素
            sb.append("[");
            sb.append(list.getList().get(0));
            sb.append(",").append(list.getList().get(list.getList().size() - 1));
            sb.append("], ");
        } else {//是集合，输出所有元素
            sb.append("{");
            for (Integer number : list.getList()) {
                sb.append(number).append(", ");
            }
            sb.append("}, ");
        }
        return sb.toString();
    }
```
如何判断是采用区间表示，还是集合表示？主要看数据元素是否相邻，如果是，则用区间表示，如果不是，则用集合表示。因此写一个函数，返回是否相邻。

```
private boolean iscos(int a, int b, int intervla) {//想邻的间隔为interval
        return (b - a == intervla);
    }
```
当遍历数组时，一个区间或集合结束，则要重新开始一个新的IntegerIntervalOrSet，因此写一个函数，完成重置。
```private IntegerIntervalOrSet endList(IntegerIntervalOrSet mylistSet, StringBuilder sb) {
        sb.append(outputList(mylistSet));
        mylistSet = new IntegerIntervalOrSet();//重新初始化
        return mylistSet;
    }
```
该任务中有两种类型的整数区间，一种是间隔为1，即2~9之间间隔为1，而10~360间隔为10。除此以外，两种类型的处理方法相同。处理思路是，如果后边的数据与当前的数据连续，则归为区间，如果不连续，则归到集合中。从原来的区间到现在的集合，或者反之，需要给出区间或集合的结束符，并重新开始一个新的IntegerIntervalOrSet对象。

```private IntegerIntervalOrSet dealSet(IntegerIntervalOrSet mylistSet, int[] a, int i, StringBuilder sb, int upbound, int step) {
        mylistSet.getList().add(a[i]);
        if (i == a.length - 1) {//数组遍历结束
            //sb.append(" and ");//在文章中，最后一种列举情况，加and
            mylistSet = endList(mylistSet, sb);
            return mylistSet;
        }
        if (a[i + 1] >= upbound) {
            mylistSet = endList(mylistSet, sb);
        }        if (iscos(a[i], a[i + 1], step)) {//连续，继续
            mylistSet.setInterval(true);
        } else {//不连续了
            if (mylistSet.isInterval()) {//说明有数据，所以要处理
                mylistSet = endList(mylistSet, sb);
            } else if (i + 2 < a.length) {
                if (iscos(a[i + 1], a[i + 2], step)) {//后边的连续了，所以要结束了
                    mylistSet = endList(mylistSet, sb);
                } else {//后边的也不连续，所以可以继续
                }
            }
        }
        return mylistSet;
    }
```
对于原数据，要分开两种情况处理，一种是大于9的，一种是小于10的。
```
private StringBuilder dealArray(int[] a) {
        StringBuilder sb = new StringBuilder();
        IntegerIntervalOrSet mylistSet = new IntegerIntervalOrSet();
        int i = 0;
        for (; i < a.length - 1; i++) {
            if (a[i] < 10) {
                mylistSet = dealSet(mylistSet, a, i, sb, 10, 1);
            } else {
                break;
            }
        }
        //下面处理大于9的情况
        for (; i < a.length - 1; i++) {
            mylistSet = dealSet(mylistSet, a, i, sb, 370, 10);
        }
        return sb;
    }
```
写一个主类Test，测试一下吧
```
public class Test {
 public static void main(String[] args) {
        int[] a = new int[]{2, 3, 4, 6, 7, 9, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 190, 200, 210, 230, 250, 260, 270, 280, 290, 300, 320, 330, 340, 350};
        Test test = new Test();
        test.dealArray(a);
        System.out.println(test.dealArray(a).toString());
}
…
}
```
用程序处理96组数据，每组不运行不超过1秒种，拷贝与粘贴假设需要29秒，那么整个数据处理需要时间只需要48分钟，不到1小时时间，最重要的是，程序不会像人一样出错。 

### 加入单目标测试函数3步骤
步骤1：写一个类在包problem.TGA.singleObjective中；
步骤2：在包problem的类FactoryProblems的方法registerProblems中注册对应的类，给出编号与名字；
步骤3：在项目的resources目录中写出对应的Controlparameters.conf配置文件。
### 可视化及其实现
工具：借助xChart

使用方法：MVC和生产者消费者设计模式。

被观察对象：Population

视图对象：tools.xChart.MyRealtimeChart01

设计方法：~在MyRealtimeChart01中注册被观察对象population（间接注册）；在population中注册视图对象(间接注册)。当数据取出后，还没有新数据之前，设置newdata=false，反之为true。MyRealtimeChart01把setData暴露给population，由population去更新数据。~

- Population（生产者）把进化数据（产品）放到队列中，xChart（消费者）从队列中取数据。为此，在ControlParameters中设置historyPopulation字段，用来保存历史种群，作为产品。在类Connection4SetAndRunning中定义方法savePopulation(Population population)用于保存进化过程中产生的数据。
- 另外，在ControlParameters中设置isOnlyBest字段，以标识是否是只需要保存最优个体。绘制最优个体进化过程图时，则只保留最优个体；绘制整个种群的分布图时，则需要显示全部个体

分工：进化过程动态显示之绘制背景，属于MVC中的M的一个组成部分，应由problem负责，所以在每个具体的problem类中，会有返回一系列均匀分布的X和Y的功能。

调用：
	执行tools.xChart.RealtimeChartSerials01，即可查看个体在目标空间中的变化情况。
 ![](figures/xyChartFunction.png)
	执行tools.xChart.RealtimeChartSerials01，即可查看最优适应值变化情况。
 ![](figures/xChartFintess.png)
### 实验出问题的特征
	求出的最大值大于规定的最大值太多，可能是测试函数的代码写错了
	实验数据显示只有从第0到第1代的适应值跃迁，测试函数中的stopFitness或函数写错了
 
## 5 使用的设计模式
### 5.1工厂模式
利用了工厂模式，处理Indivdiual、进化算子，包括选择、交叉与变异、适应值赋值方式、Population、处理编码与解码、Problem、Phenotype等。

PSO和GA两者都属于启发式算法，有许多共同的部分，就像宝马和奥迪一样，都属于车，有许多共同的部分，只不过组装时采用不同的品牌罢了。

系统通过接口定义选择、交叉和变异这3个算子作为遗传算法的成员，具体对象从他们对应的Factory中获取。而这3个算子的输出都是调用接口中的方法，具体实现则由具体的对象完成。
 
## 6 面向对象编程
### 6.1 参数粒度大小
定义方法时，对象参数既不要太大，也不要太小。太小好理解，太大则是常常容易被忽视的问题。

参数对象太小的例子。你开了一个修理铺，专营负责修改座椅。有一天有个人拿了一个木棍来，请你修理，说这是座椅的腿，你可能告诉他，如果腿掉下来了，可以粘上去，但不负责木棍的磨削刨油漆等。

参数太大的例子。又有一天，有个人开轿车来了，请你修理车上的座椅，你说好的，放这儿吧，明天或后来你来拿。注意，是让他来拿他的座椅，而不是轿车。轿车这个参数就太大了，因为你在修改座椅的过程中，会占有车，很多其他的业务会因此受影响，甚至不小心，把车的轮胎扎破了，把引擎碰坏了。又有一天，有个人开着飞机来了，让你修理座椅，这个对象参数就更大了。

对于进化算子，有人说其作用的对象是种群。虽然这种说法没错，但是还是太大，可以再小些，例如个体集合。因为种群中除了个体集合外，还包括最大适应值，最小适应值等多个属性。在进化操作处理过程中，一不小心，可能就把他们破坏了。
### 6.2 父类或抽象避免越俎代庖
定义一个接口的抽像类，把公共的方法都在抽象类中实现。但是需要注意的是，不要定义太多的成员属性，尤其不要把子类的所有成员属性都定义在抽象类中。因为如果是这要，无关的子类也会变得庞大。如果这些属性是被引用的，则可能会被子类修改；如果这些属性是被重新实例化的，则可能实例化后并不使用，带来无谓的空间浪费。

举例：在本项目中对于Individual的抽象类AbstractIndividual的定义。又如individual是否要定义mutationScope的讨论。每个individual都有一个变异范围，但大多数情况下，这个变异范围对同一问题是固定不变的（但也有例外，如人脸）。如果放进individual类中，这个变异范围的数组就会重复出现多次，尤其当种群规模很大时，将大大无谓增加内存负担。

解决办法之一：上例中，要使用变异范围，怎么办？传递一个problem引用过来，注意，这里传递的是引用，而不是对象值，引用意味着只是指向一个内存空间。
### 6.3抽象类不能通过this来调用自己
抽象类不能通过this来调用自己，因为运行时其具体子类未知。在本项目中，在接口Problem中有一个方法getIndividual(int individualNumber,Problem problem);注意到这里的第2个参数是problem，如果在抽象类AbstractProblem中实现该方法，并传递参数this作为problem的具体值，那么运行时将产生错误，而且对于这种错误，系统目前也很难定位；也就是说，可能产生一个语法上和逻辑上完全正确，但运行时却出错，且位置未知的错误。这种错误可能派1000人也不一定能检查出来。 但是采用这种方法会产生一个奇怪的调用 problem.getIndividual(1, problem)，即problem自己调用自己时，需要把自己作为参数传进来。原因是调用的是父类的方法，是告诉父类：“我来了”
### 6.4 对象的引用与赋值
**如果需要赋值，则要避免引用**

例一：为什么在许多set方法中用了System.arraycopy方法？这是因为对数组直接赋值，是地址引用赋值，而非值赋值。当其他的对象对该数组做了修改后，也会影响到引用它的对象，这与程序的初衷是背离的。因为我们当初赋值后，是不希望被其他对象影响的，即保持独立性，而不是值的高耦合性。
### 6.5 关于Java的克隆
当所写类是面向类似于泛型所写，即不知道当前类型时什么时，如面向Individual所写，不知具体是FloatIndivdiual或StringIndividual或BianrycodeIndividual等时，利用当前population.get(0).getClone()，然后再setGenecodes或setGenecodes是一个好方法。这样编程，也同时可以避免判断问题是哪一种类型了

当然，对于某些情况下，个体的初始化，可以直接用Clone方法。

这个例子较好地解释了克隆和hashSet
~~~
package problem.Individuals;

import controllers.ControlParameters;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import problem.population.DoublePopulation;
import tools.MyMath.RandomGenerator;

/**
 *
 * @author 郝国生 HAO Guo-Sheng
 */
public class DoubleIndividual extends AbstractIndividual {
//
//    @Override
//    public Individual copyXTo() {//不拷贝适应值，节约内存空间
//        DoubleIndividual individual = null;
//        try {
//            individual = (DoubleIndividual) this.clone();
//            individual.setFitness(null);
//        } catch (CloneNotSupportedException ex) {
//            Logger.getLogger(DoubleIndividual.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return individual;
//    }

    @Override
    public boolean equals(Object obj) {
        if (null == obj || !(obj instanceof DoubleIndividual)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        //每个决策变量都相等，才相等;而没考虑适应值
        int i = 0;
        DoubleIndividual tem = (DoubleIndividual) obj;
        for (int j = 0; j < getGeneCodes().length; j++) {
            if (tem.getGeneCodes()[j] == this.getGeneCodes()[j]) {
                i++;
            } else {
                break;
            }
        }
        return i >= getGeneCodes().length;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 61).
                // if deriving:appendSuper(super.hashCode()).
                append(this.getGeneCodes()).
                //append(this.getFitness()).
//                append(Math.random()).//这所以加这个，就是想要克隆变为深克隆，而不是浅克隆
                toHashCode();
    }
    @Override
    public GenecodeType getGenecodeType() {
        return GenecodeType.DOUBLECODE;//表示实数编码
    }

    public double getAdjusted(double value, int location, double[][] scope) {
        double result = value;
        if (value > scope[location][0]) {//超过了上限
            result = scope[location][0] - RandomGenerator.nextDouble();
        }
        if (value < scope[location][1]) {//低于了下限
            result = scope[location][1] + RandomGenerator.nextDouble();
        }
        return result;
    }

    @Override
    public String getX() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < getGeneCodes().length; i++) {
            result.append(getGeneCodes()[i]);
        }
        return result.toString();
    }

    @Override
    public String getGeneralGenecodeString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < geneCodes.length; i++) {
            result.append(geneCodes[i]);
        }
        return result.toString();
    }

    @Override
    public int getSolutionNumberDistance(Individual individual) {
        int result = 1;
        for (int i = 0; i < this.getGeneCodes().length; i++) {
            result *= Math.abs(this.getGeneCodes()[i] - ((DoubleIndividual) individual).getGeneCodes()[i]) / ControlParameters.currentProblem.getVariableProperties()[i][2];
        }
        //System.out.println("距离是:"+result);
        return result;
    }

    @Override
    public int compareToX(Individual o) {//在搜索空间中进行折半查找时用
        for (int i = 0; i < this.getGeneCodes().length; i++) {
            if (this.getGeneCodes()[i] > o.getGeneCodes()[i]) {
                return 1;
            } else if (this.getGeneCodes()[i] < o.getGeneCodes()[i]) {
                return -1;
            }
        }
        return 0;//两者相等，是同一个解
    }

    @Override
    public List<Individual> crossover(Individual crossed) {
        List<Individual> result = null;
        try {
            result = new LinkedList<>();
            //定义两个结果的clone
            Individual individual1 = (DoubleIndividual) this.clone();
            Individual individual2 = (DoubleIndividual) crossed.clone();
            //修改其基因编码
            double[][] crossresult = swapDoubleArray(individual1.getGeneCodes(), individual2.getGeneCodes());
            individual1.setGeneCodes(crossresult[0]);
            individual2.setGeneCodes(crossresult[1]);
            //把结果加入
            result.add(individual1);
            result.add(individual2);

        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(DoubleIndividual.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public double[][] swapDoubleArray(double[] currentDoubleArray0, double[] currentDoubleArray1) {
        int doubleLength = currentDoubleArray0.length;//基因变量个数
        double[][] result = new double[2][doubleLength];//2表示交叉后得到两个个体
        if (doubleLength > 1) {
            for (int j = 0; j < ControlParameters.crossoverPointNum; j++) {//
                int temInt = RandomGenerator.getRandom(0, doubleLength);
                if (temInt == 0) {
                    temInt++;
                }
                //先拷贝前半部分
                System.arraycopy(currentDoubleArray0, 0, result[0], 0, temInt);
                System.arraycopy(currentDoubleArray1, 0, result[1], 0, temInt);
                //再拷贝后半部分
                System.arraycopy(currentDoubleArray0, temInt, result[1], temInt, currentDoubleArray0.length - temInt);
                System.arraycopy(currentDoubleArray1, temInt, result[0], temInt, currentDoubleArray1.length - temInt);//
            }

        } else {//决策变量只有一个,//验证结果在这两个数值的中间
            double max = Math.max(currentDoubleArray0[0], currentDoubleArray1[0]);
            double min = Math.min(currentDoubleArray0[0], currentDoubleArray1[0]);
            if (max == min) {//不进行任何操作，因为两者相等
                result[0] = currentDoubleArray0;
                result[1] = currentDoubleArray1;
            } else {//这种方法只适合于凸问题
                double error = max - min;
                result[0][0] = min + RandomGenerator.nextDouble() * error;
                result[1][0] = max - RandomGenerator.nextDouble() * error;
            }
        }
        return result;
    }

    @Override
    public Individual mutate() {
        Individual result = this.cloneWithFitness();
        if (this.getGeneCodes().length > 2) {
            //首先确定变异的决策变量
            double[] temDoubleArray = new double[this.getGeneCodes().length];//不能直接引用this.getGeneCodes，因为那样就变成引用了
            System.arraycopy(this.getGeneCodes(), 0, temDoubleArray, 0, this.getGeneCodes().length);
            int muteLocation = (int) RandomGenerator.getRandom(0, temDoubleArray.length - 1);
            temDoubleArray[muteLocation] = this.getGeneCodes()[muteLocation] + RandomGenerator.nextDouble();
            temDoubleArray[muteLocation] = this.getAdjusted(temDoubleArray[muteLocation], muteLocation, ControlParameters.currentProblem.getVariableProperties());
            result.setGeneCodes(temDoubleArray);//不用设置，也会改变，因为数组是引用传递
        } else {// 只有一个或两个决策变量，让第一个进行变异
            double[] temDoubleArray = new double[this.getGeneCodes().length];//不能直接引用this.getGeneCodes，因为那样就变成引用了
            System.arraycopy(this.getGeneCodes(), 0, temDoubleArray, 0, this.getGeneCodes().length);
            temDoubleArray[0] = this.getGeneCodes()[0] + RandomGenerator.nextDouble();
            temDoubleArray[0] = this.getAdjusted(temDoubleArray[0], 0, ControlParameters.currentProblem.getVariableProperties());
            result.setGeneCodes(temDoubleArray);
        }
        return result;
    }
    //下面这个运行证明了equals中对比个体编码方法的有效性，即在HashSet中remove方法时利用了equal，对比编码，而不是在对象层次

    public static void main(String[] args) {
        HashSet<DoubleIndividual> setIndviduals = new HashSet<>();
        DoubleIndividual tem = new DoubleIndividual();
        tem.setGeneCodes(new double[]{0.5});
        tem.setFitness(new double[]{1.0});
        DoubleIndividual aClone = null, bClone = null;
        try {
            aClone = (DoubleIndividual) tem.clone();
            bClone = (DoubleIndividual) tem.clone();
            setIndviduals.add((DoubleIndividual) tem.clone());
            setIndviduals.add(aClone);
            setIndviduals.add(bClone);
            System.out.println("setIndviduals的size:" + setIndviduals.size());//结果显示为1
            bClone.setFitness(new double[]{0.8});
            setIndviduals.add(bClone);
            /*
               @Override
    public boolean equals(Object obj) {
        if (null == obj || !(obj instanceof DoubleIndividual)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        //每个决策变量都相等，才相等;而没考虑适应值
        int i = 0;
        DoubleIndividual tem = (DoubleIndividual) obj;
        for (int j = 0; j < getGeneCodes().length; j++) {
            if (tem.getGeneCodes()[j] == this.getGeneCodes()[j]) {
                i++;
            } else {
                break;
            }
        }
        return i >= getGeneCodes().length;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 61).
                // if deriving:appendSuper(super.hashCode()).
                append(this.getGeneCodes()).
                append(this.getFitness()).//
//                append(Math.random()).//这所以加这个，就是想要克隆变为深克隆，而不是浅克隆
                toHashCode();
    }
            */
             System.out.println("setIndviduals的size:" + setIndviduals.size());//结果显示为2，表明在hashSet中未使用equal函数，而是使用了hashcode函数，因为按照equal函数的话，他们是相等的，但使用hashcode函数的话，他们是不相等的；正是因为不相等，所以结果为2
             //由此可知，hashCode函数是为HashSet服务的，而HashSet与equals函数不同
             //所以，如果想在hashSet只判断决策变量的话，hashCode应该写成只判断决策变量的方法，所以hashcode变成如下的样子。则结果变成了1，表明两个决策变量相同
             /*
             @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 61).
                // if deriving:appendSuper(super.hashCode()).
                append(this.getGeneCodes()).
                //append(this.getFitness()).
//                append(Math.random()).//这所以加这个，就是想要克隆变为深克隆，而不是浅克隆
                toHashCode();
    }
             */
            setIndviduals.add(bClone);
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(DoublePopulation.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("aClone.equals(tem):" + aClone.equals(tem));//检查equals,结果显示结果为true,即两者内容一样
        System.out.println("aClone==tem:" + (aClone == tem));//检查==，结果显示结果为false，即两者不是同一个对象
        //修改了原来的对象
        tem.setGeneCodes(new double[]{0.9});
        tem.setFitness(new double[]{1.2});
        //检查一下克隆是否被改变
        setIndviduals.forEach((doubleIndividual) -> {
            System.out.println(doubleIndividual.getGeneCodes()[0] + ":" + doubleIndividual.getFitness()[0]);
        });
        //结果显示，克隆没有改变，即对母体tem做改变，不影响clone，即clone不改变
        //生成一个新的对象，但和原来的一模一样，但不是一个
        tem = new DoubleIndividual();
        tem.setGeneCodes(new double[]{0.5});
        tem.setFitness(new double[]{1.0});
        System.out.println("Before add remove:");
        setIndviduals.remove(tem);
        System.out.println("After add remove:");
        System.out.println(setIndviduals.size());
        setIndviduals.forEach((doubleIndividual) -> {
            System.out.println(doubleIndividual.getGeneCodes()[0] + ":" + doubleIndividual.getFitness()[0]);
        });
    }

}
~~~
### 6.6 Java的数据结构TreeSet
~利用TreeSet的上界与下界功能
使用TreeSet的celling和floor，从历史个体中获得对应的邻居，然后计算该个体是否落在这些邻居排斥半径内。
下面是一个简单的例子。~
~~~
public static void main(String[] args) {
        // create tree set object 
            TreeSet<DecisionVariable> treeadd = new TreeSet<>(); 
  
            // populate the TreeSet 
            treeadd.add(new DoubleDecisionVariable(new double[]{10.0})); 
            treeadd.add(new DoubleDecisionVariable(new double[]{20.0})); 
            treeadd.add(new DoubleDecisionVariable(new double[]{30.0})); 
            treeadd.add(new DoubleDecisionVariable(new double[]{40.0})); 
  
            // Print the TreeSet 
            System.out.println("TreeSet: " + treeadd); 
  
            // getting ceiling value for 25 
            // using ceiling() method 
            DecisionVariable tem=new DoubleDecisionVariable(new double[]{25.0});
            DecisionVariable value = treeadd.ceiling(tem); 
            DecisionVariable value2=treeadd.floor(tem);
  
            // printing  the ceiling value 
            System.out.println("Ceiling value for 25: "
                               + value+"floor value for 25 is:"+value2); 
    }
~~~
~~ 聚类--利用TreeSet进行处理
在利用历史个体库的过程中，为了防止发生内存溢出的错误，要及时删除一些个体。那么删除哪些个体？一种方法是基于拥挤crowded原则的删除，这时就需要计算个体决策变量之间的距离，从这些距离中挑选最小距离对应的一个个体删除。而在新加入一个个体时，需要计算该个体与所有个体之间的距离，而这种计算其实会重复TreeSet的计算，因为TreeSet加入新的个体时会进行计算，能否利用TreeSet避免或减少距离计算？这就是利用TreeSet进行处理的来由。
现在采用的方法是：只求相邻个体的距离。这依赖于一个假设：对TreeSet进行遍历时，是依据compare()结果从小到大进行的，即是按序遍历。这样，就只需要计算遍历相邻的两个个体即可。当用于parentFilter时，保留适应值大的（当用于offspringFilter时，保留适应值小的，目前还没用到这种情况，因为offspring排斥时，用的是距离排斥）。 
为此，需要一个包含属性（DecisionVariable,DecsionVariable, Double）的类，保存变量之间的距离。因为DecisionVariable采用引用，所以并不会太增加内存空间。再将这个结构的变量放在TreeHashSet中，每次删除则弹出first，从中删除适应值小的DecsionVariable即可。 
~~
# FAQ
1. Q:如何查看当前的Problem?
	A:在
2. 
