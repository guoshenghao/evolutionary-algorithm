package core.entrance;

import core.algorithm.DecisionVariables.GenecodeType;
import core.algorithm.GeneticAlgorithm;
import core.controllers.ControlConstantParameters;
import core.controllers.ControlDynamicParameters;
import core.problem.FactoryProblems;

/**
 *
 * @author 郝国生 HAO Guo-Sheng
 */
public class MainCmdRuning {
    //从命令行直接进入
    protected String setupFileName;
    
    public static void main(String[] args) {
        new MainCmdRuning().mainBody();
        System.exit(0);
    }

    public String getSetupFileName() {
        return ControlConstantParameters.setupFileName;
    }

    public void setup() {
        //对于非界面运行情况，从文件中读取配置内容
        ControlConstantParameters.fp.readFromFile(getSetupFileName());
        ControlDynamicParameters.crossoverType = Integer.parseInt(ControlConstantParameters.fp.getValue("crossoverType"));
        FactoryProblems.dimension = Integer.parseInt(ControlConstantParameters.fp.getValue("dimensionNumber"));
        ControlDynamicParameters.crossoverPointNum = FactoryProblems.dimension == 1 ? 1 : FactoryProblems.dimension - 1;
        //FactoryMutation.mutationPointNum = FactoryProblems.dimensionNumber == 1 ? 1 : (FactoryProblems.dimensionNumber - 1);
        ControlDynamicParameters.mutationType = Integer.parseInt(ControlConstantParameters.fp.getValue("mutationType"));
        ControlDynamicParameters.selectionType = Integer.parseInt(ControlConstantParameters.fp.getValue("selectionType"));
        ControlConstantParameters.stopEvaluationNumber = Integer.parseInt(ControlConstantParameters.fp.getValue("stopEvaluationNumber"));
        ControlDynamicParameters.populationSize = Integer.parseInt(ControlConstantParameters.fp.getValue("populationsize"));
        ControlDynamicParameters.crossMutationCompeteProbability = Double.parseDouble(ControlConstantParameters.fp.getValue("crossMutationCompeteProbability"));
        ControlDynamicParameters.genecodeType=(GenecodeType.valueOf(ControlConstantParameters.fp.getValue("genecodeType")));
         if (null != ControlDynamicParameters.currentAlgorithm.getObserver().getEvolutionaryDataQueue()) {
              ControlConstantParameters.experimentRunningTime = 1;
         }else{
              ControlConstantParameters.experimentRunningTime = Integer.parseInt(ControlConstantParameters.fp.getValue("experimentRunningTime"));
         }
    }

    public void mainBody() {
        FactoryProblems.registerProblems();
        ControlConstantParameters.fp.readFromFile(getSetupFileName());
        this.mainrun();
    }

    protected void mainrun() {
        FactoryProblems.problemNum = Integer.parseInt(ControlConstantParameters.fp.getValue("problemNum"));
        FactoryProblems.dimension = Integer.parseInt(ControlConstantParameters.fp.getValue("dimensionNumber"));
        FactoryProblems.initProblem(FactoryProblems.problemNum,ControlDynamicParameters.genecodeType);
        //从配置文件中读内容进来
        ControlDynamicParameters.currentAlgorithm = new GeneticAlgorithm();
        ControlDynamicParameters.currentAlgorithm.init();
        this.setup();
        //if (temKnowledge != Knowledge.SimpleGA) {//历史个体不重复出现
        //交叉与变异必须执行，不然的话，历史个体就会重复出现
        // FactoryCrossover.crossProbability = 1f;
        //FactoryMutation.mutationProbability = 1f;
        //}
        //ControlParameters.fp.readFromFile(getSetupFileName());
        Connection4SetAndRunning connection4SetAndRunning = new Connection4SetAndRunning();
        connection4SetAndRunning.run();
    }

}
