package core.problem.TGA.singleObjective;

import core.algorithm.DecisionVariables.DoubleDecisionVariable;
import core.problem.FactoryProblems;
import core.problem.Individual;
import core.problem.Problem;

/**
 *
 * @author 郝国生 HAO Guo-Sheng
 */
public class F13  extends Problem<DoubleDecisionVariable> {
    @Override
    public F13 init(int dimension) {
        super.init(dimension);this.dimension = dimension;
        stopFitness = new double[]{0};
        variableProperties = new double[dimension][3];
        for (int i = 0; i < dimension; i++) {
            variableProperties[i] = new double[]{10, -10,0.0001};
        }
        return this;
    }

    @Override
    public void evaluate(Individual inputedIndividual) {//传过来的是DoubleIndividual类型，即决策变量是浮点型
        double sum=0, product=1;
        for (int i = 0; i < dimension; i++) {
            sum+=Math.abs(inputedIndividual.getDecisionVariable().getGeneCodes()[i]);
            product*=Math.abs(inputedIndividual.getDecisionVariable().getGeneCodes()[i]);
        }
        inputedIndividual.getDecisionVariable().setFitness(new double[]{-(sum+product)});//把求最小值问题转换为求最大值问题了
    }

    @Override
    public boolean isIECProblem() {
        return false;
    }

    @Override
    public String getName() {
        return  FactoryProblems.getName(13, dimension);
    }
}
