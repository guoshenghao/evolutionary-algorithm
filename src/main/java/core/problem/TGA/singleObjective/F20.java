package core.problem.TGA.singleObjective;

import core.algorithm.DecisionVariables.DoubleDecisionVariable;
import core.problem.FactoryProblems;
import core.problem.Individual;
import core.problem.Problem;

/**
 *
 * @author 郝国生 HAO Guo-Sheng
 */
public class F20 extends Problem<DoubleDecisionVariable> {

    @Override
    public F20 init(int dimension) {
        super.init(dimension);this.dimension = dimension;
        stopFitness = new double[]{0};
        variableProperties = new double[dimension][3];
        for (int i = 0; i < dimension; i++) {
            variableProperties[i] = new double[]{30, -30,0.0001};
        }
        return this;
    }

    @Override
    public void evaluate(Individual inputedIndividual) {//传过来的是DoubleIndividual类型，即决策变量是浮点型
        double fitness = 0;
        for (int i = 1; i < dimension; i++) {
            double tem=inputedIndividual.getDecisionVariable().getGeneCodes()[i] -(inputedIndividual.getDecisionVariable().getGeneCodes()[i-1]*inputedIndividual.getDecisionVariable().getGeneCodes()[i-1]);
            fitness += 100*tem*tem+(inputedIndividual.getDecisionVariable().getGeneCodes()[i-1]-1)*(inputedIndividual.getDecisionVariable().getGeneCodes()[i-1]-1);
        }
        inputedIndividual.getDecisionVariable().setFitness(new double[]{-fitness});//把求最小值问题转换为求最大值问题了
    }

    @Override
    public boolean isIECProblem() {
        return false;
    }

    @Override
    public String getName() {
        return  FactoryProblems.getName(20, dimension);
    }
}
