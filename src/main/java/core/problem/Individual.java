package core.problem;

import core.algorithm.DecisionVariables.DecisionVariable;
import core.algorithm.DecisionVariables.DoubleDecisionVariable;
import java.util.Arrays;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * 继承该类，有两个方法必须要实现：equal()和getClone()
 *
 * @author 郝国生 HAO Guo-Sheng
 */
public class Individual implements Comparable<Individual>, Cloneable {

    DecisionVariable decisionVariable;
    //protected double[] fitness;//可以是一维的，也可以是多维的,具体维数要等到执行时具体指定
    //private int mutationNumber;
    private int index;//在粒子群算法等群智能算法中，要使用某个粒子的编号
    //private int explo_ra_itationFact = 1;
    //实现无级的（需要为double类型，这里采用了int，就是有级的）exploration和exploitation的切换
    //其默认值1表示不进行exploeration或exploitation的调节
    //当这个值大于1时，则是在执行explorer

    public Individual(DecisionVariable decisionVariable) {
        this.decisionVariable = decisionVariable;
        //mutationNumber = FactoryProblems.currentProblem.getDimension();
    }

    @Override
    public int compareTo(Individual o) {//求最大化的问题
        int ge = 0, le = 0;
        for (int i = 0; i < this.decisionVariable.getFitness().length; i++) {
            if(this.decisionVariable==null||this.decisionVariable.getFitness()==null){
                System.out.println("");
            }
              if(o.decisionVariable==null||o.decisionVariable.getFitness()==null){
                System.out.println("");
            }
            if (this.decisionVariable.getFitness()[i] < o.decisionVariable.getFitness()[i]) {
                le--;
            } else if (this.decisionVariable.getFitness()[i] > o.decisionVariable.getFitness()[i]) {
                ge++;
            }
        }
        if (ge == this.decisionVariable.getFitness().length) {//所有this的目标值都优于o的目标值
            return 1;
        } else if (le == -this.decisionVariable.getFitness().length) {//所有this的目标值都劣于o的目标值
            return -1;
        } else {//两者适应值相等或不可比
            return 0;
        }
    }

    public double getDistanceWithFitness(Individual another) {
        double result = 0.0;
        for (int i = 0; i < this.decisionVariable.getFitness().length; i++) {
            result += Math.pow(this.decisionVariable.getFitness()[i] - another.decisionVariable.getFitness()[i], 2);
        }
        return Math.sqrt(result);
    }

    @Override
    public boolean equals(Object object) {
        boolean result = false;
        if (null == object || !(object instanceof Individual)) {
            return false;
        }
        if (object == this) {
            return true;
        }
        //先比较适应值，如果相等，则比较决策变量，如果决策变量也相等，则相等
        //比起维数动辄几十维的决策变量空间，适应值空间的维数往往较低
        int equalCount = 0;
        if (null != this.decisionVariable.getFitness()) {
            equalCount = this.compareTo((Individual) object);
            if (equalCount == 0) {//适应值相等，下面看决策变量是否相等
                result = this.getDecisionVariable().equals(((Individual) object).getDecisionVariable());
            }
        } else {//适应值不可用，则用决策变量
            result = this.getDecisionVariable().equals(((Individual) object).getDecisionVariable());
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return new HashCodeBuilder(17, 61).
                append(71 * hash + Objects.hashCode(this.decisionVariable)).
                append(71 * hash + Arrays.hashCode(this.decisionVariable.getFitness())).
                toHashCode();
    }

    @Override
    public Individual clone() throws CloneNotSupportedException {
        Individual result = null;
        try {
            super.clone();
            result = new Individual((DecisionVariable) decisionVariable.clone());
            if (null != this.decisionVariable.getFitness()) {
                double[] localfitness = new double[this.decisionVariable.getFitness().length];
                System.arraycopy(this.decisionVariable.getFitness(), 0, localfitness, 0, localfitness.length);
                result.decisionVariable.setFitness(localfitness);
            }
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Individual.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public DecisionVariable getDecisionVariable() {
        return this.decisionVariable;
    }

    public void setDecisionVariable(DecisionVariable decisionVariable) {
        this.decisionVariable = decisionVariable;
    }

    public Individual mutate() {
        Individual ind = null;
        try {
            ind = this.clone();
            ind.getDecisionVariable().mutate();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Individual.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ind;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        String tem = "";
        for (int i = 0; i < decisionVariable.getFitness().length; i++) {
            tem += decisionVariable.getFitness()[i];
        }
        return decisionVariable.toString() + "\t" + tem;
    }

    public static void main(String[] args) {//检测compareto是否存在问题
        double[] fitness = {
            1.987237548,
            1.987237548,
            2.866935216,
            1.987237548,
            2.866935216,
            2.243063332,
            2.866935216};
        Individual[] individuals = new Individual[fitness.length];
        for (int i = 0; i < fitness.length; i++) {
            individuals[i] = new Individual(new DoubleDecisionVariable());
            double[] fit = {fitness[i]};
            individuals[i].getDecisionVariable().setFitness(fit);
        }
        for (int i = 1; i < fitness.length; i++) {
            if(individuals[i].compareTo(individuals[i - 1]) * (fitness[i] - fitness[i - 1]) < 0){
                System.out.println("sfgfsdfsd");
            }
        }
    }

}
