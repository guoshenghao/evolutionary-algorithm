package core.problem;

import core.algorithm.DecisionVariables.AbstractDecisionVariable;
import core.algorithm.DecisionVariables.DecisionVariable;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

/**
 *
 * @author 郝国生 HAO Guo-Sheng
 * @param <T>
 */
public class Population<T extends AbstractDecisionVariable> {

    protected List<Individual> individuals;//依据目标变量和决策变量在两个空间排序
    // protected TreeSet<DecisionVariable> LastPopulationDecisonVariables = new TreeSet<>();//上一代的种群，可以在进入下一代前，直接被当代替换
    //protected double[] fitness4Selection;//用于保存为选择算子服务的临时适应值
    double volume;
    private Individual bestIndividual, worstIndividual;
    public List<Individual> getIndividuals() {
        return individuals;
    }

    public void setIndividuals(List listIndividuals) {
        this.individuals = listIndividuals;
    }

    public List<Individual> init(int indivdiualNumber) {
        //生成individuals
        //这里之所以不赋值给本population，是因为进化过程中，需要生成1或2个新的补充个体
        List<Individual> listIndividuals = new LinkedList<>();
        while (listIndividuals.size() < indivdiualNumber) {//因为使用TreeSet数据结构，所以不会有重复个体
            Individual ind = new Individual(FactoryProblems.currentProblem.getDecisionVariable().initVariable());
            listIndividuals.add(ind);
        }
        return listIndividuals;
    }

    protected TreeSet<DecisionVariable> decisionVariables = new TreeSet<>();

    public double getVolume() {
        decisionVariables.clear();
        individuals.forEach((ind) -> {
            decisionVariables.add(ind.getDecisionVariable());
        });
        volume = 1;
        switch (FactoryProblems.currentProblem.getDecisionVariable().getGenecodeType()) {
            case DOUBLECODE:
                for (int i = 0; i < decisionVariables.last().getGeneCodes().length; i++) {
                    volume *= Math.pow(decisionVariables.last().getGeneCodes()[i] - decisionVariables.first().getGeneCodes()[i],
                            1.0 / decisionVariables.last().getGeneCodes().length);
                }
                break;
            case STRINGCODE:
                for (int i = 0; i < decisionVariables.last().getGenecodesString().length(); i++) {
                    volume *= Math.pow(decisionVariables.last().getGenecodesString().charAt(i)
                            - decisionVariables.first().getGenecodesString().charAt(i), 1 / decisionVariables.last().getGenecodesString().length());
                }
                break;
            case AFFINEFACECODE:
                System.out.println("还不支持");
                break;
            case BINARYCODE:
                System.out.println("还不支持");
                break;
            case NORMALFACECODE:
                System.out.println("还不支持");
                break;
        }
        return volume;
    }

 

    public Individual getBestIndividual() {
        return bestIndividual;
    }

    public void setBestIndividual(Individual bestIndividual) {
        this.bestIndividual = bestIndividual;
    }

    public Individual getWorstIndividual() {
        return worstIndividual;
    }

    public void setWorstIndividual(Individual worstIndividual) {
        this.worstIndividual = worstIndividual;
    }
}
