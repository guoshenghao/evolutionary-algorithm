package core.problem.Decoder;

import core.algorithm.DecisionVariables.GenecodeType;

/**
 *
 * @author 郝国生 HAO Guo-Sheng
 */
public class FactoryDecoder {

    String[] virtualCodeStr;
    double[] virtualcodeDouble;
    double[] realcode;

    public FactoryDecoder(String[] virtualcodeStr) {
        this.virtualCodeStr = virtualcodeStr;
    }

    public FactoryDecoder(double[] virtualcodeDouble) {
        this.virtualcodeDouble = virtualcodeDouble;
    }

    public double[] getRealCodes(GenecodeType genecodeType) {
        BaseDecoder bed = null;
        switch (genecodeType) {
            case BINARYCODE://基因编码是字符串型
                bed = new BinaryDeCoder();
                bed.setVirtualCodeStr(virtualCodeStr);
                break;
            case DOUBLECODE:
                break;
        }
        bed.decode();
        return bed.getRealCodes();
    }
}
