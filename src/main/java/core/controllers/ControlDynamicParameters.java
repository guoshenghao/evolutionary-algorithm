package core.controllers;

import core.algorithm.Algorithm;
import core.algorithm.DecisionVariables.GenecodeType;

/**
 *
 * @author 郝国生
 */
public class ControlDynamicParameters {

    //这里的参数，是控制器中的可调参数。可以由观测器观测算法状态后，进行调节
    public static int populationSize;//种群规模的下限与上限
    public static int upperBoundPopulationSize;//种群规模的下限与上限
    public static GenecodeType genecodeType;
    public static Algorithm currentAlgorithm;
    public static int algorithmNumber;//算法对应的编号 
    public static double crossMutationCompeteProbability;//60%交叉,40%变异
    public static int crossoverType;//交叉算子类型
    public static int crossoverPointNum;//交叉点的个数，如单点交叉、两点交叉等
    public static double crossProbability = 0.8;//交叉概率
    public static double mutationProbability = 0.1;//变异概率
    public static int mutationType;//变异类型
    public static int selectionType;//选择算子类型


}
