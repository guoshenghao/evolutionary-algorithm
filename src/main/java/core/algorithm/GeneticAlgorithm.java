package core.algorithm;

import core.algorithm.operator.Crossover.Crossover;
import core.algorithm.operator.Crossover.FactoryCrossover;
import core.algorithm.operator.Mutation.FactoryMutation;
import core.algorithm.operator.Mutation.Mutation;
import core.algorithm.operator.Selection.FactorySelection;
import core.algorithm.operator.Selection.Selection;
import core.problem.Individual;
import core.problem.Population;
import java.util.List;

/**
 *
 * @author 郝国生 HAO Guo-Sheng
 */
public class GeneticAlgorithm extends AbstractAlgorithm {

    Crossover crossover;
    Mutation mutation;
    Selection selection;
    //HashSet保存交叉与变异个体的索引号
    List<Individual> crossedIndividuals, mutationIndividuals;

    //List保存交叉与变异的个体
    public GeneticAlgorithm() {
        super();
        crossover = FactoryCrossover.getCrossover();
        mutation = FactoryMutation.getMutation();
        selection = FactorySelection.getSelection();
    }

    @Override
    public List<Individual> operatePopulation(Population pop) {
//最优个体保留到新的一代的目的是在选择算子中使用，所以在选择前执行，至于从外观指标上来说，则是用于监控算法用的
        List<Individual> result = mutation.getMutationResult(
                crossover.getCrossoverResult(
                        selection.getSelectionResult(pop)
                )
        );
        return result;
    }

    @Override
    public String getName() {
        return "Genetic Algorithm";
    }
}
