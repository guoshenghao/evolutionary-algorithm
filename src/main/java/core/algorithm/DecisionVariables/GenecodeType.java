package core.algorithm.DecisionVariables;

/**
 *
 * @author hgs07
 */
public enum GenecodeType {
    BINARYCODE, DOUBLECODE, STRINGCODE,NORMALFACECODE,AFFINEFACECODE,INTGENECODE;
}
