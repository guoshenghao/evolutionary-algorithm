package core.algorithm;

/**
 *
 * @author 郝国生 HAO Guo-Sheng
 */
public class FactoryAlgorithm {

    public static Algorithm getAlgorithm(int type) {
        switch (type) {
            case 0:
                return new GeneticAlgorithm();
            default:
                return null;
        }
    }
}
