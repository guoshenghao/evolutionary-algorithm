package core.algorithm.operator.Crossover;

import core.problem.Individual;
import java.util.List;

/**
 *
 * @author 郝国生 HAO Guo-Sheng
 */
public interface Crossover {

    public List<Individual> getCrossoverResult(List<Individual> inputtedIndividual);

//    public float getCrossoverProbability();
//
//    public void setCrossoverProbability(float crossoverProbability);
//
//    public int getCrossoverNumber();
//
//    public void setCrossoverNumber(int crossoverNumber);

}
