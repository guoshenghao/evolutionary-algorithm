package core.algorithm.operator.Crossover;

import core.controllers.ControlDynamicParameters;
import core.problem.Individual;
import java.util.List;

/**
 *
 * @author 郝国生 HAO Guo-Sheng
 */
public class FactoryCrossover {
    public static Crossover getCrossover(){
        Crossover result=null;
        switch (ControlDynamicParameters.crossoverType) {
            case 0:
                result= new RegularCrossover();
        }
        return result;
    }

    public static List<Individual> getRegularCrossoveredPopulation(List<Individual> inputtedIndividual) {
        RegularCrossover rc = new RegularCrossover();
        return rc.getCrossoverResult(inputtedIndividual);
    }

    public static String getName(int type) {
        switch (type) {
            case 0:
                return "RegularCrossover";
            default:
                return "RegularCrossover";
        }
    }
}
