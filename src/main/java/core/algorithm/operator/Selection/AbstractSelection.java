package core.algorithm.operator.Selection;

import core.problem.Individual;
import core.problem.Population;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class AbstractSelection implements Selection {

    @Override
    public List<Individual> getSelectionResult(Population population) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    private double[] fitness;
    private List<Individual> individualList;

    public void prepareCandadateList(Population population) {
        fitness = new double[population.getIndividuals().size()];
        individualList = new ArrayList<>();
        int i = 0;
        for (Object ind : population.getIndividuals()) {
            fitness[i] = ((Individual) ind).getDecisionVariable().getFitness()[0];
            individualList.add((Individual) ind);
        }
    }

    public double[] getFitness() {
        return fitness;
    }

    public List<Individual> getIndividualList() {
        return individualList;
    }

    public boolean continueSelection(Population population) {
        double fmax =  population.getBestIndividual().getDecisionVariable().getFitness()[0],
                fmin = population.getWorstIndividual().getDecisionVariable().getFitness()[0];
        //计算正弦适应值与适应值的和
        return fmax!= fmin;//两者相等
            //不用选择了，交给交叉与变异去处理
    }
}
