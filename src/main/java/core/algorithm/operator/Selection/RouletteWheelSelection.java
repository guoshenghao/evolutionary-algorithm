package core.algorithm.operator.Selection;

import core.problem.Individual;
import core.problem.Population;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author 郝国生 HAO Guo-Sheng
 */
public class RouletteWheelSelection extends AbstractSelection {

    @Override
    public List<Individual> getSelectionResult(Population population) {
        if (continueSelection(population)) {
            prepareCandadateList(population);
            return new SelectionCommonFunction().getSelectionResult(getIndividualList(), getFitness());
        } else {
            return new LinkedList<>(population.getIndividuals());
        }
    }
}
