package core.algorithm.operator.Selection;

import core.controllers.ControlConstantParameters;
import core.problem.Individual;
import core.problem.Population;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author 郝国生 HAO Guo-Sheng
 */
public class RankSelection extends AbstractSelection {

    @Override
    public List<Individual> getSelectionResult(Population population) {
        if (continueSelection(population)) {
            double[] probability;
            prepareCandadateList(population);
            if (ControlConstantParameters.rankProba == null) {//如果没有给出rank的比例，则在这里给出
                probability = new double[getIndividualList().size()];
                double divisor = ((double) (getIndividualList().size() - 1) * (double) getIndividualList().size());
                for (int i = 0; i < probability.length; i++) {
                    probability[i] = (2.0 * i) / divisor;
                }
            } else {
                probability = ControlConstantParameters.rankProba;
            }
            return new SelectionCommonFunction().getSelectionResult(getIndividualList(), probability);//只适用于单目标
        } else {
            return new LinkedList<>(population.getIndividuals());
        }
    }
}
