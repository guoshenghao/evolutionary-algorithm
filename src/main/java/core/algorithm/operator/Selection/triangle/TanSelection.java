package core.algorithm.operator.Selection.triangle;

import core.algorithm.operator.Selection.AbstractSelection;
import core.algorithm.operator.Selection.SelectionCommonFunction;
import core.problem.Individual;
import core.problem.Population;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author 郝国生 HAO Guo-Sheng
 */
public class TanSelection extends AbstractSelection {

    @Override
    public List<Individual> getSelectionResult(Population population) {
        if (continueSelection(population)) {
            double fmax = population.getBestIndividual().getDecisionVariable().getFitness()[0],
                    fmin = population.getWorstIndividual().getDecisionVariable().getFitness()[0];
            prepareCandadateList(population);
            double[] virtualFitness = new double[population.getIndividuals().size()];
            int i = 0;
            for (Individual ind : getIndividualList()) {
                virtualFitness[i++] = Math.tan(
                        Math.PI / 4 * ((((Individual) ind).getDecisionVariable().getFitness()[0] - fmin)
                        / (fmax - fmin)));
            }
            return new SelectionCommonFunction().getSelectionResult(getIndividualList(), virtualFitness);
        } else {
            return new LinkedList<>(population.getIndividuals());
        }
    }
}
