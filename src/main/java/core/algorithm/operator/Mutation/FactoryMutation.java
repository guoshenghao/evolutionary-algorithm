package core.algorithm.operator.Mutation;

import core.controllers.ControlDynamicParameters;

/**
 *
 * @author 郝国生 HAO Guo-Sheng
 */
public class FactoryMutation {

    public static Mutation getMutation() {
        Mutation result = null;
        switch (ControlDynamicParameters.mutationType) {
            case 0:
                result = new RegularMutation();
        }
        return result;
    }

    public static String getName(int type) {
        switch (type) {
            case 0:
                return "Regular Mutation";
            default:
                return "Regular Mutation";
        }
    }

}
