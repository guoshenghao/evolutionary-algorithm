package core.algorithm;

import core.observer.Observer;
import core.problem.Individual;
import core.problem.Population;
import java.util.List;

/**
 *
 * @author 郝国生 HAO Guo-Sheng
 */
public interface Algorithm {

    public void init();

    public void operats(Population populations);// 完成对解的更新

    public String getName();

    //public void onceGetFitness4TEC();
    public List<Individual> operatePopulation(Population pop);

    //public void outputBest(Individual best, int i);
    
    public void onceGetFitness4TEC(Population population);
    
    public Observer getObserver();
}
